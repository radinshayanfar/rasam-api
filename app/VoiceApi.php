<?php

namespace App;

use GuzzleHttp\Client;

class VoiceApi
{
    private static $url;

    public static function config($url)
    {
        self::$url = $url;
    }

    public static function verify($new_audio, $samples)
    {
        $client = new Client();

        $multipart = [
            [
                'name' => 'audio',
                'contents' => $new_audio,
                'filename' => 'audio.mp3',
            ]
        ];
        foreach ($samples as $index => $sample) {
            $multipart[] = [
                'name' => "sample$index",
                'contents' => $sample,
                'filename' => "$index.mp3",
            ];
        }

        $response = $client->request('POST', self::$url . '/verify', [
            'multipart' => $multipart,
        ]);

        return json_decode($response->getBody());
    }
}
