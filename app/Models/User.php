<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'fname',
        'lname',
        'email',
        'gender',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
//    protected $hidden = [
//        'password',
//        'remember_token',
//    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
//    protected $casts = [
//        'email_verified_at' => 'datetime',
//    ];

    public static function register(Request $request)
    {
        $voices = collect($request->get("voices"))->map(function ($item, $key) {
            return base64_decode($item);
        });

        $user = null;
        DB::transaction(function () use (&$user, $request, $voices) {
            $user = User::create($request->except("voices"));
            $id = $user->id;
            $paths = $voices->map(function ($item, $key) use ($id) {
                $path = "voices/$id-$key.mp3";
                Storage::put($path, $item);
                return ["path" => $path];
            });
            $user->voices()->createMany($paths);
        });

        return $user;
    }

    public function createApiToken()
    {
        // Revoking all tokens
        $this->tokens()->delete();

        return $this->createToken('api-token')->plainTextToken;
    }

    public function voices()
    {
        return $this->hasMany(Voice::class);
    }
}
