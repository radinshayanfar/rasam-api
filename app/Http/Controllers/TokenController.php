<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTokenRequest;
use App\Models\User;
use App\Traits\ApiResponder;
use App\VoiceApi;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Storage;

class TokenController extends Controller
{
    use ApiResponder;

    public function store(StoreTokenRequest $request)
    {
        $user = User::where('email', $request->get('email'))->firstOrFail();
        $voices = $user->voices->map(function ($item, $key) {
            return Storage::get($item->path);
        })->toArray();

        $verification = VoiceApi::verify(base64_decode($request->get('audio')), $voices)->prediction;
        if ($verification) {
            $apiToken = $user->createApiToken();
            return $this->success(compact('apiToken'), 'Logged in.', 201);
        } else {
            return $this->failure('Authentication failed.', 401);
        }
    }

    public function destroy(Authenticatable $user)
    {
        $user->currentAccessToken()->delete();

        return $this->success(null, "Logged out.", 200);
    }
}
