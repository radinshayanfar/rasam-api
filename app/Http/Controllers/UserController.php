<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Traits\ApiResponder;
use Illuminate\Contracts\Auth\Authenticatable;

class UserController extends Controller
{
    use ApiResponder;

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $user = User::register($request);
        $apiToken = $user->createApiToken();

        return $this->success(compact(["user", "apiToken"]), "User created.", 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Authenticatable $user)
    {
        return $this->success($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, Authenticatable $user)
    {
        $user->update($request->all());

        return $this->success($user, "User updated successfully.", 200);
    }
}
