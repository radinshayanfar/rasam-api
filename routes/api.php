<?php

use App\Http\Controllers\TokenController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('guest:sanctum')->group(function () {
    // Register route
    Route::post('/users', [UserController::class, 'store'])->name('users.store');
    Route::post('/tokens', [TokenController::class, 'store'])->name('tokens.store');

});

Route::middleware('auth:sanctum')->group(function () {
    // Logout route
    Route::delete('/tokens', [TokenController::class, 'destroy'])->name('tokens.destroy');

    Route::name('users.')->group(function () {
        Route::get('/users', [UserController::class, 'show'])->name('show');;
        Route::match(['put', 'patch'], '/users', [UserController::class, 'update'])->name('update');
    });
});
